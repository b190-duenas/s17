console.log("hello world");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function information(){
	let fullName = prompt("What is your name?");
	let age = prompt("How old are you?");
	let location = prompt("Where do you live?");
	alert("Thank you for your input");

	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);
};

information();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function favoriteMusician(){
	console.log("1. Nirvana");
	console.log("2. Red hot chili pepper");
	console.log("3. Starting line");
	console.log("4. Dashboard Confessional");
	console.log("5. All American Reject");
}

favoriteMusician();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies(){
	console.log("1. Apocalypto");
	console.log("Rotten Tomatoes Rating: 67%");
	console.log("2. Avengers: Endgame");
	console.log("Rotten Tomatoes Rating: 97%");
	console.log("3. Spider-man (2002)");
	console.log("Rotten Tomatoes Rating: 90%");
	console.log("4. Bohemian Rhapsody");
	console.log("Rotten Tomatoes Rating: 60%");
	console.log("5. Spider-man 3 (2007)");
	console.log("Rotten Tomatoes Rating: 63%");
};

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
